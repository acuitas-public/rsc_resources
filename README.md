# RSC Resources

- Who: Acuitas Health
- Where: Here
- When: Now
- What: Collection of resources that are useful for RStudio Connect Projects
- How: Git
- Why: We need a common place to store images, CSS, etc. which we will use in RSC projects.
    - Question: Why not a CDN? 
    - Answer: For Markdown, setting up a CDN with RSC is a PITA. It builds projects 
      to rely on local resources. In other words, it will copy these resources into projects.
    - Answer 2: For formal SHINY apps, I need to do more research.

# Resources

- [Images](./images/)
- [CSS](./css/)

Other resources, etc. will be added with time.
